#!/usr/bin/php
<?PHP

# IMPORTANT!! jsub -l release=trusty

$api_url = 'https://www.wikidata.org/w/api.php' ;

ini_set('user_agent','Magnus labs tools'); # Fake user agent

require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/wikidata-todo/vendor/mwapi/vendor/autoload.php' ) ;

$file = $argv[1] ;
if ( !isset($file) ) die ( "Requires filename as parameter!" ) ;

$last_type = '' ;
$last_created_q = '' ;
$wil = new WikidataItemList ;

function getAPI () {
	global $api , $api_url ;
	if ( isset($api) and $api->isLoggedin() ) return $api ;
	$config = parse_ini_file ( '/data/project/wikidata-todo/reinheitsgebot.conf' ) ;
	$api = new \Mediawiki\Api\MediawikiApi( $api_url );
	if ( !$api->isLoggedin() ) {
		$x = $api->login( new \Mediawiki\Api\ApiUser( $config['user'], $config['password'] ) );
		if ( !$x ) return false ;
	}
	return $api ;
}


function getParams ( $p , $v ) {
	global $last_type ;


	if ( preg_match ( '/^"(.+)"$/i' , $v , $m ) ) {
		$last_type = 'string' ;
		return $m[1] ;
	}

	if ( preg_match ( '/^([a-z.-]+):"(.+)"$/i' , $v , $m ) ) {
		$last_type = 'monolingualtext' ;
		return array ( "text" => $m[2] , "language" => $m[1] ) ;
	}
	
	if ( preg_match ( '/^Q(\d+)$/i' , $v , $m ) ) {
		$last_type = 'wikibase-entityid' ;
		return array ( "entity-type" => 'item' , "numeric-id" => $m[1] , 'id' => 'Q'.$m[1] ) ;
	}

	if ( preg_match ( '/^\@([-0-9.]+?)\/([-0-9.]+?)$/' , $v , $m ) ) {
		$last_type = 'globecoordinate' ;
		return array ( "latitude" => $m[1]*1 , "longitude" => $m[2]*1 , "globe" => "http://www.wikidata.org/entity/Q2" , "precision" => 0.000001 ) ;
	}


	if ( preg_match ( '/^([+-]{0,1})0*(\d+-\d\d-\d\dT\d\d:\d\d:\d\dZ)/' , $v , $m ) ) {
		$last_type = 'time' ;
		if ( $m[1] == '' ) $m[1] = '+' ;
		$precision = 0 ;
		if ( preg_match ( '/\/(\d+)$/' , $v , $n ) ) $precision = $n[1] ;
		return array ( "time" => $m[1].$m[2] , "timezone"=>0 , "before"=>0 , "after"=>0 , "precision" => $precision*1 , "calendarmodel" => "http://www.wikidata.org/entity/Q1985727" ) ;
	}
	
	
	if ( preg_match ( '/^([+-]{0,1})(\d+)$/i' , $v , $m ) ) {
		$last_type = 'quantity' ;
		$n = $m[1] == '-' ? '-'.($m[2]*1) : '+'.($m[2]*1) ;
		return array ( "amount" => $n , "upperBound" => $n , "lowerBound" => $n  ) ;
	}

	if ( preg_match ( '/^([+-]{0,1})(\d+):(Q\d+)$/i' , $v , $m ) ) {
		$last_type = 'quantity' ;
		$n = $m[1] == '-' ? '-'.($m[2]*1) : '+'.($m[2]*1) ;
		return array ( "amount" => $n , "upperBound" => $n , "lowerBound" => $n  , 'unit' => "http://www.wikidata.org/entity/".$m[3] ) ;
	}

}

function checkStatementExists ( $q , $p , $j ) {
	global $wil ;
	$id = 0 ;
	$wil->loadItem ( $q ) ;
	$i = $wil->getItem ( $q ) ;
	$claims = $i->getClaims ( $p ) ;
	
	foreach ( $claims AS $c ) {
		if ( !isset($c->mainsnak) ) continue ;
		if ( !isset($c->mainsnak->datavalue) ) continue ;
		if ( !isset($c->mainsnak->datavalue->value) ) continue ;
		$value = $c->mainsnak->datavalue->value ;
		if ( is_object ( $value ) and is_array ( $j ) ) {
			$mismatch = false ;
			foreach ( $j AS $k => $v ) {
#				print "COMPARING: $k / " . $value->$k . " / $v\n" ;
				if ( isset($value->$k) and $value->$k == $v ) continue ;
#				print "MISMATCH: $k / " . $value->$k . " / $v\n" ;
				$mismatch = true ;
				break ;
			}
			if ( !$mismatch ) return $c->id ;
		} else if ( $value == $j ) return $c->id ;
	}
	
	return $id ;
}

function postAction ( $action , $params , $id = 0 ) {
	global $last_exception ;
	$api = getAPI() ;
	$params['token'] = $api->getToken() ;
	$params['bot'] = 1 ;
	$params['summary'] = '#quickstatements' ;

	try {
		$x = $api->postRequest( new \Mediawiki\Api\SimpleRequest( $action, $params ) );
		if ( isset($x) ) {
			if ( $action == 'wbeditentity' and isset($params['new']) and isset($x['entity']) and isset($x['entity']['id']) ) $id = $x['entity']['id'] ;
			else if ( isset($x) and isset($x['claim']) and isset($x['claim']['id']) ) $id = $x['claim']['id'] ;
		}
	} catch (Exception $e) {
		$last_exception = $e ;
		echo "Caught exception for $action/$id: ",  $e->getMessage(), "\n";
//		print_r ( $params ) ;
//		$x = false ;
		return false ;
	}
	return $id ;
}

function addStatement ( $q , $p , $v ) {
	$id = 0 ;
	$j = getParams ( $p , $v ) ;
	if ( !isset ( $j ) ) {
		print "PARSE FAIL: $q / $p / $v\n" ;
		return $id ;
	}
	
	$id = checkStatementExists ( $q , $p , $j ) ;
	if ( $id ) return $id ;

	$params = array (
		'value' => json_encode ( $j ) ,
		'entity' => $q ,
		'property' => $p ,
		'snaktype' => "value"
	) ;

	$id = postAction ( 'wbcreateclaim' , $params , $id ) ;

	return $id ;
}


function addLDA ( $q , $p , $v , $action ) {
	if ( !preg_match ( '/^"(.+)"$/' , $v , $m ) ) {
		print "UNQUOTED TEXT: $q / $p / $v\n" ;
		return 0 ;
	}
	$text = $m[1] ;

	if ( !preg_match ( '/^[LDSA]([a-zA-Z-]{2,})$/' , $p , $m ) ) {
		print "BAD LANGUAGE: $q / $p / $v\n" ;
		return 0 ;
	}
	$lang = strtolower ( $m[1] ) ;
	
	
	$params = array (
		'id' => $q ,
		'language' => $lang
	) ;
	
	if ( $action == 'wbsetaliases' ) $params['add'] = $text ;
	else $params['value'] = $text ;

	$id = postAction ( $action , $params ) ;
	return $id ;
}

function addLabel ( $q , $p , $v ) {
	return addLDA ( $q , $p , $v , 'wbsetlabel' ) ;
}

function addAlias ( $q , $p , $v ) {
	return addLDA ( $q , $p , $v , 'wbsetaliases' ) ;
}

function addDescription ( $q , $p , $v ) {
	return addLDA ( $q , $p , $v , 'wbsetdescription' ) ;
}

function addSitelink ( $q , $p , $v ) {
	if ( !preg_match ( '/^"(.+)"$/' , $v , $m ) ) {
		print "UNQUOTED TEXT: $q / $p / $v\n" ;
		return 0 ;
	}
	$text = $m[1] ;

	if ( !preg_match ( '/^[LDS]([a-zA-Z_-]{2,})$/' , $p , $m ) ) {
		print "BAD SITE: $q / $p / $v\n" ;
		return 0 ;
	}
	$site = strtolower ( $m[1] ) ;

	$params = array (
		'id' => $q ,
		'linksite' => $site ,
		'linktitle' => $text
	) ;
	
	$id = postAction ( 'wbsetsitelink' , $params ) ;
	return $id ;
}

function addInformation ( $q , $p , $v ) {
#	print "Using: $q/$p/$v\n" ;
	if ( preg_match ( '/^P\d+$/' , $p ) ) return addStatement ( $q , $p , $v ) ;
	if ( preg_match ( '/^L.+$/' , $p ) ) return addLabel ( $q , $p , $v ) ;
	if ( preg_match ( '/^D.+$/' , $p ) ) return addDescription ( $q , $p , $v ) ;
	if ( preg_match ( '/^A.+$/' , $p ) ) return addAlias ( $q , $p , $v ) ;
	if ( preg_match ( '/^S.+$/' , $p ) ) return addSitelink ( $q , $p , $v ) ;
	return 0 ;
}


function addSource ( $id , $p , $v ) {
	global $last_type ;

	$j = getParams ( $p , $v ) ;
	if ( !isset ( $j ) ) {
		print "PARSE FAIL: $id / $p / $v\n" ;
		return ;
	}
	$p = preg_replace ( '/^S/' , 'P' , $p ) ;
	$jp = (object) array () ;
	$jp->$p = array ( (object) array ( "snaktype"=>"value" , "property"=>$p , "datavalue"=>(object) array ( "type" => $last_type , "value"=>$j ) ) ) ;
	
	$params = array (
		'statement' => $id ,
		'snaks' => json_encode ( $jp )
	) ;

	postAction ( 'wbsetreference' , $params ) ;
}

function addQualifier ( $id , $p , $v ) {
	$j = getParams ( $p , $v ) ;
	if ( !isset ( $j ) ) {
		print "PARSE FAIL: $id / $p / $v\n" ;
		return ;
	}

	$params = array (
		'claim' => $id ,
		'property' => $p ,
		'value' => json_encode ( $j ) ,
		'snaktype' => "value"
	) ;
	return postAction ( 'wbsetqualifier' , $params ) ;
}

function addAux ( $id , $p , $v ) {
	if ( !isset($id) or $id == '0' ) return ; // PARANOIA: BAD ID
#	print "AUX: $id/$p/$v\n" ;
	if ( preg_match ( '/^S\d+$/' , $p ) ) return addSource ( $id , $p , $v ) ;
	if ( preg_match ( '/^P\d+$/' , $p ) ) return addQualifier ( $id , $p , $v ) ;
}

function createItem ( $data ) {
	global $last_created_q , $creation_cache , $is_creating , $last_exception ;
//print_r ( $data ) ;exit(0);
	$cnt = 0 ;
	do {
		if ( $cnt > 1 ) break ;
		$cnt++ ;
		$params = array (
			'new' => 'item' ,
			'data' => json_encode ( (object) $data )
		) ;
		$last_created_q = postAction ( 'wbeditentity' , $params ) ;

		if ( $last_created_q === false ) {
			$t = $last_exception->getMessage() ;
			if ( preg_match ( '/associated with language code (.+),/' , $t , $m ) ) {
				$lang = $m[1] ;
				$data['descriptions']['en']['value'] .= '.' ;
			} else break ; // Something else is wrong
		} else print "$last_created_q\n" ;
	} while ( $last_created_q === false ) ;
	
	$is_creating = false ;
	$creation_cache = array() ;
}

function mergeItems ( $q1 , $q2 ) { // UNTESTED!!
	$params = array (
		'fromid' => strtoupper(trim($q1)) ,
		'toid' => strtoupper(trim($q2)) ,
		'ignoreconflicts' => 'description'
	) ;
	postAction ( 'wbmergeitems' , $params ) ;
}

function removeStatement ( $parts ) {
	$q = $parts[0] ;
	$p = $parts[1] ;
	$v = $parts[2] ;
	$j = getParams ( $p , $v ) ;
	if ( !isset ( $j ) ) {
		print "PARSE FAIL: $q / $p / $v\n" ;
		return $id ;
	}
	
	$id = checkStatementExists ( $q , $p , $j ) ;
	if ( !$id ) return ; // No such statement
	
	$params = array ( 'claim' => $id ) ;
	postAction ( 'wbremoveclaims' , $params ) ;
}


function add2creation ( $parts ) {
	global $creation_cache , $last_type ;
	array_shift ( $parts ) ; // LAST
	$p = trim ( array_shift ( $parts ) ) ;
	$v = trim ( array_shift ( $parts ) ) ;
	
	$sm = array() ; // Statement
	
	if ( preg_match ( '/^(L|A|D|S)(.+)$/' , $p , $m ) ) {
		$mode = $m[1] ;
		$lang = $m[2] ;
		$v = preg_replace ( '/^\s*"\s*/' , '' , $v ) ;
		$v = preg_replace ( '/\s*"\s*$/' , '' , $v ) ;
		if ( $mode == 'L' ) $creation_cache['labels'][$lang] = array ( 'language' => $lang , "value" => $v ) ;
		if ( $mode == 'D' ) $creation_cache['descriptions'][$lang] = array ( 'language' => $lang , "value" => $v ) ;
		if ( $mode == 'A' ) $creation_cache['aliases'][$lang][] = array ( 'language' => $lang , "value" => $v ) ;
		if ( $mode == 'S' ) $creation_cache['sitelinks'][$lang] = array ( 'site' => $lang , "title" => $v , "badges" => array() ) ; // lang=wiki
		return true ;
	} else if ( preg_match ( '/^P\d+$/' , $p ) ) {
		
		$dv = getParams ( $p , $v ) ;
		$sm = array (
			'mainsnak' => array (
				'snaktype' => 'value' ,
				'property' => strtoupper($p) ,
				'datavalue' => array (
					'value' => $dv ,
					'type' => $last_type
				)
			) ,
			'type' => 'statement' ,
			'rank' => 'normal'
		) ;
		
	}
	
	while ( count($parts) >= 2 ) {
		$p = strtoupper ( trim ( array_shift ( $parts ) ) ) ;
		$v = array_shift ( $parts ) ;
		
		if ( preg_match ( '/^S(\d+)$/' , $p , $m ) ) { // Source
		
			$p = 'P' . $m[1] ;
			$dv = getParams ( $p , $v ) ;
			$sm['references'][0]['snaks'][$p][] = array (
				'snaktype' => 'value' ,
				'property' => strtoupper($p) ,
				'datavalue' =>array (
					'value' => $dv ,
					'type' => $last_type
				)
			) ;
			
		} else { // Qualifier
			$dv = getParams ( $p , $v ) ;
			$sm['qualifiers'][$p][] = array (
				'snaktype' => 'value' ,
				'property' => strtoupper($p) ,
				'datavalue' => array (
					'value' => $dv ,
					'type' => $last_type
				)
			) ;
		}
	}

	$creation_cache['claims'][] = $sm ;
	
	return true ;
}


//_________________________________________________________________________________________________________

$is_creating = false ;
$creation_cache = array() ;

$rows = explode ( "\n" , file_get_contents ( $file ) ) ;
foreach ( $rows AS $row ) {
	$row = trim ( $row ) ;

	if ( strtoupper($row) == 'CREATE' ) {
		if ( $is_creating ) createItem ( $creation_cache ) ;
		$is_creating = true ;
		continue ;
	}
	
	if ( $row == '' or preg_match ( '/^#/' , $row ) ) continue ;
	
	$parts = explode ( "\t" , $row ) ;
	if ( count($parts) < 3 ) continue ;


	// Add to creation cache, or create item from cache?
	if ( strtoupper(trim($parts[0])) == 'LAST' and $is_creating ) {
		if ( !add2creation ( $parts ) ) {
			print "Cannot add \"$row\" to creation!\n" ;
		}
		continue ;
	} else if ( $is_creating ) {
		createItem ( $creation_cache ) ;
	}

	// Remove ?
	if ( preg_match ( '/^-/' , $row ) ) {
		if ( count($parts) == 3 ) {
			$parts[0] = trim ( substr($parts[0],1) ) ;
			removeStatement ( $parts ) ;
		} else {
			print "BAD REMOVE: $row\n" ;
		}
		continue ;
	}


	// Merge?
	if ( strtoupper($row) == 'MERGE' ) {
		mergeItems ( $parts[1] , $parts[2] ) ;
		continue ;
	}
	
	if ( strtoupper(trim($parts[0])) == 'LAST' ) $q = $last_created_q ;
	else $q = 'Q' . preg_replace ( '/\D/' , '' , $parts[0] ) ;
	if ( $q == 'Q' ) continue ;
	
	$p = strtoupper ( trim ( $parts[1] ) ) ;
	$v = trim ( $parts[2] ) ;
	
#$q = 'Q4115189' ; # TESTING
	$id = addInformation ( $q , $p , $v ) ;
	if ( !isset($id) or $id == '0' ) continue ;
	
	for ( $pos = 3 ; $pos+1 < count($parts) ; $pos += 2 ) {
		$p = strtoupper ( trim ( $parts[$pos] ) ) ;
		$v = trim ( $parts[$pos+1] ) ;
		addAux ( $id , $p , $v ) ;
	}
}

if ( $is_creating ) createItem ( $creation_cache ) ;

?>
