#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$lang = 'en' ;
$stringent = false ;

$wiki = $lang . 'wiki' ;
$db = openToolDB ( 'duplicity_p' ) ;

$pages = array() ;
$sql = "SELECT * FROM no_wd WHERE wiki='$wiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$t = str_replace ( ' ' , '_' , $o->title ) ;
	$pages[] = $db->real_escape_string ( $t ) ;
}

$db2 = openDBwiki ( $wiki ) ;
$sql = 'select distinct page_title,c1.cl_to AS born,c2.cl_to AS died FROM page,categorylinks c1,categorylinks c2 WHERE page_namespace=0 and page_id=c1.cl_from and page_id=c2.cl_from and c1.cl_to like "_____births" and c2.cl_to like "_____deaths"' ;
$sql .= " AND page_title IN ('" . implode("','",$pages) . "')" ;
#$sql .= " LIMIT 20" ; // TESTING FIXME
unset ( $pages ) ;
$pages = array() ;
if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	if ( !preg_match ( '/^\d\d\d\d_births$/' , $o->born ) ) continue ;
	if ( !preg_match ( '/^\d\d\d\d_deaths$/' , $o->died ) ) continue ;
	$t = str_replace ( '_' , ' ' , $o->page_title ) ;
	$b = preg_replace ( '/\D/' , '' , $o->born ) ;
	$d = preg_replace ( '/\D/' , '' , $o->died ) ;
	$pages[] = array ( $t , $b , $d ) ;
}

$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
foreach ( $pages AS $p ) {
	$t = preg_replace ( '/ \(.+\)$/' , '' , $p[0] ) ;
	$b = $p[1] ;
	$d = $p[2] ;
	
	$candidates1 = array() ;
	$sql = "SELECT distinct term_entity_id FROM wb_terms WHERE term_entity_type='item' AND term_type IN ('label','alias') AND term_text='" . $dbwd->real_escape_string($t) . "'" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_item_id=term_entity_id LIMIT 1)" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$candidates1[] = $o->term_entity_id ;
	}
	if ( count($candidates1) == 0 ) continue ;

	if ( $stringent ) {
		$query = "between[569,$b,$b-13] and between[570,$d,$d-13] and items[" . implode(',',$candidates1) . "]" ; # Stringent
	} else {
		$query = "(between[569,$b,$b-13] or between[570,$d,$d-13]) and items[" . implode(',',$candidates1) . "]" ; # Fuzzy
	}
	$url = "$wdq_internal_url?q=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	if ( count($j->items) != 1 ) continue ;

	$q = 'Q'.$j->items[0] ;

	if ( $stringent ) {
		print "$q\tP31\tQ5\n" ;
		print "$q\tL$lang\t\"$t\"\n" ;
		print "$q\tS$wiki\t\"" . $p[0] . "\"\n" ;
	} else {
		print "<li><a href='//$lang.wikipedia.org/wiki/" . urlencode($p[0]) . "' target='_blank'>$t</a> might be <a href='//www.wikidata.org/wiki/$q' target='_blank'>$q</a></li>\n" ;
	}
	

}

?>