#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
ini_set('display_errors', 'On');
ini_set('memory_limit','1000M');

//include_once ( '../public_html/php/common.php' ) ;
include_once ( 'generate_statements.inc' ) ;

$outfile = "out.tab" ;

$items = array() ;

if ( false ) { // Use query

//	$query = "claim[31:5] and claim[214] and claim[227] and noclaim[569] and noclaim[570]" ;
//	$query = "claim[31:5] and claim[214] and noclaim[569] and noclaim[570]" ;
//	$query = "claim[31:5] and noclaim[214] and noclaim[106] and noclaim[569] and noclaim[570] and link[dewiki] and link[enwiki]" ;
	$query = "claim[31:5] and noclaim[569] and link[dewiki,enwiki]" ;
	print "Running query \"$query\"\n" ;
	$url = $wdq_internal_url . "?q=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$items = $j->items ;

} else { // Use item list

	print "Loading item list\n" ;
	$l = file_get_contents ( 'itemlist.tab' ) ;
	$items = explode ( "\n" , $l ) ;

}

print "Scanning " . count ( $items ) . " items...\n" ;
unlink ( $outfile ) ;
//exec ( "touch $outfile" ) ;

$fp = fopen($outfile, 'w');
foreach ( $items AS $k => $q ) {
//	if ( $q == 'Q2089809' ) exit ( 0 ) ; // ONE-TIME HACK

	$r = run_gs ( "$q" ) ;
	foreach ( $r AS $l ) {
		fwrite($fp, "$l\n");
	}

//	exec ( "./generate_statements.php $q >> $outfile" ) ;
//	if ( $k > 10 ) exit ( 0 ) ; // TESTING
}
fclose($fp) ;

?>