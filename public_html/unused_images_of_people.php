<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;

$depth = 2 ;

$db = openDB ( 'commons' , 'wikimedia' ) ;

$cats = array() ;
findSubcats ( $db , array('People') , $cats , $depth ) ;

print get_common_header ( '' , 'Unused images of people' ) ;
#print "<pre>" ; print_r ( $cats ) ; print "</pre>" ; 

$sql = "SELECT DISTINCT page_title FROM page,categorylinks WHERE cl_from=page_id AND page_namespace=6 AND cl_to IN ('" . implode("','",$cats) . "')" ;
$sql .= " AND NOT EXISTS (SELECT * FROM globalimagelinks WHERE page_title=gil_to LIMIT 1)" ;
$sql .= " AND page_is_redirect=0" ;
$sql .= " LIMIT 100" ;

if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	print "<p>" . $o->page_title . "</p>" ;
//	$ret[$o->page_title] = $o->page_title ;
}


?>