<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
include_once ( 'php/common.php' ) ;

$action = get_request('action','') ;
$db = openDB ( 'wikidata' , 'wikidata' ) ;
$out = array ( 'status' => 'OK' ) ;

if ( $action == 'check_titles_aliases' ) {
	$title = get_request('title','###THIS TITLE DOES NOT EXIST') ;
	$title = trim ( str_replace ( '_' , ' ' , $title ) ) ;
	$ts = $db->real_escape_string ( $title ) ;
	$out['data'] = array() ;
	$sql = "select distinct term_entity_id from wb_terms where term_entity_type='item' and term_type IN ('label','alias') AND term_text='$ts'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][] = 'Q' . $o->term_entity_id ;
	}
}

header('Content-type: application/json');
print json_encode ( $out ) ;

?>