<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;

print get_common_header ( '' , 'Important blank items' ) ;
print "<p>Items that do have many language links (main namespace only), but do not reference other items. Top 50.000, updated hourly.</p>" ;

$start = get_request ( 'start' , 0 ) * 1 ;
$size = get_request ( 'size' , 100 ) * 1 ;

function navlinks ( $top ) {
	global $start , $size ;
	$r = rand ( 0 , 50000-$size ) ;
	$r -= $r % $size ;
	$h = "<div style='border-" . ($top?'bottom':'top') . ":1px solid black;text-align:center'>" ;
	if ( $start > 0 ) {
		$h .= "<a href='?start=" . ($start-$size) . "'>" . ($start-$size+1) . "-" . ($start) . "</a> | " ;
	}
	$h .= ($start+1) . "-" . ($start+$size) . " | " ;
	$h .= "<a href='?start=" . ($start+$size) . "'>" . ($start+$size+1) . "-" . ($start+$size*2) . "</a> | " ;
	$h .= "<a href='?start=$r'>Random</a>" ;
	$h .= "</div>" ;
	return $h ;
}

$fh = fopen ( '/data/project/wikidata-todo/no_items.50K.tab' , 'r' ) ;
for ( $i = 0 ; $i <= $start ; $i++ ) fgets ( $fh ) ;
for ( $i = 0 ; $i < $size ; $i++ ) $lines[] = fgets ( $fh ) ;
fclose ( $fh ) ;

$q = array() ;
$item_ids = array() ;
foreach ( $lines AS $k => $l ) {
	if ( $l == '' ) continue ;
	$a = explode ( "\t" , $l ) ;
	$item = 'Q' . $a[0] ;
	$q[] = $item ;
	$item_ids[] = $a[0] ;
}

$ignore = array() ;
$db = openDB ( 'wikidata' , '' ) ;
$sql = "select page_title,count(*) AS cnt from page,pagelinks where page_namespace=0 AND pl_namespace=0 AND page_title IN ('" . implode ( "','" , $q ) . "') AND pl_from=page_id group by page_id" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$ignore[$o->page_title] = $o->cnt ;
}

$languages = array ( 'en' , 'de' , 'es' , 'fr' , 'it' ) ;
$langs = array() ;
$sql = "SELECT * FROM wb_terms WHERE term_type='label' AND term_entity_id IN ('" . implode ( "','" , $item_ids ) . "') AND term_language IN ('" . implode ( "','" , $languages ) . "')" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$langs['Q'.$o->term_entity_id][$o->term_language] = $o->term_text ;
}

print navlinks(true) ;

print "<table class='table table-condensed table-striped'>" ;
print "<thead><tr><th>#</th><th>Item</th><th>Interwiki links</th></tr></thead><tbody>" ;
foreach ( $lines AS $k => $l ) {
	if ( $l == '' ) continue ;
	$a = explode ( "\t" , $l ) ;
	$item = 'Q' . $a[0] ;
	$iwlinks = $a[1] ;
	$label = $item ;
	if ( isset ( $langs[$item] ) ) {
		foreach ( $languages AS $l ) {
			if ( !isset ( $langs[$item][$l] ) ) continue ;
			$label = $langs[$item][$l] ;
			break ;
		}
	}
	
	print "<tr>" ;
	print "<td>" . ( $k + $start + 1 ) . "</td>" ;
	if ( isset ( $ignore[$item] ) ) {
		print "<td><s><b>$label</b></s> <i>" . $ignore[$item] . " item link(s) added in the last hour</i></td>" ;
	} else {
		print "<td><a href='//www.wikidata.org/wiki/$item' target='_blank'>$label</a></td>" ;
	}
	print "<td>$iwlinks</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;

print navlinks(false) ;

print get_common_footer() ;

?>