<?PHP

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
header('Content-type: application/json');
$db = openToolDB ( 'duplicity_p' ) ;
$callback = $_REQUEST['callback'] ;
$out = array () ;
$testing = isset($_REQUEST['testing']) ;

if ( $_REQUEST['action'] == 'desc' ) {
	$out = array (
		"label" => array ( "en" => 'Match new articles to items' ) ,
		"description" => array ( "en" => "Decide if a Wikipedia article and a Wikidata item describe the same entity. Using data from the duplicity tool." ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Grant_DeVolson_Wood_-_American_Gothic.jpg/100px-Grant_DeVolson_Wood_-_American_Gothic.jpg' ,
		'options' => array (
			array ( 'name' => 'Wiki' , 'key' => 'wikitype' , 'values' => array ( 'lang' => 'Your main language' , 'species' => 'WikiSpecies' , 'commons' => 'Wikimedia Commons' ) )
		)
	) ;
} else if ( $_REQUEST['action'] == 'tiles' ) {
	$num = get_request('num',0)*1 ; // Number of games to return
	$lang = get_request('lang','en') ;
	$wikitype = get_request('wikitype','lang') ;
	$wiki = preg_replace('/[^a-z_-]/','',$lang) . 'wiki' ;
	if ( $wikitype == 'species' ) $wiki = 'specieswiki' ;
	if ( $wikitype == 'commons' ) $wiki = 'commonswiki' ;
	$hadthat = explode ( ',' , preg_replace ( '/[^0-9,]/' , '' , get_request ( 'in_cache' , '' ) ) ) ;
	if ( count($hadthat)==1 and $hadthat[0] == '' ) $hadthat = array() ;
	
	$sql = "SELECT count(*) AS cnt FROM candidates,no_wd WHERE checked=0 and wiki='$wiki' AND no_wd.id=no_wd_id" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$o = $result->fetch_object() ;
	$out['left'] = $o->cnt ;
	if ( $out['left'] < 200 ) $out['low'] = 1 ;
	
	$out['tiles'] = array() ;
	for ( $n = 1 ; $n <= $num ; $n++ ) {
		$r = rand() / getrandmax() ;
		$sql = "select *,candidates.id AS cid FROM candidates,no_wd WHERE no_wd.id=no_wd_id and checked=0 and random>$r and wiki='$wiki' " ; // IN ('" . implode ( "','" , $wikis ) . "') " ;
		if ( count ( $hadthat ) > 0 ) $sql .= " AND candidates.id NOT IN (" . implode(',',$hadthat) . ") " ;
		$sql .= " order by random limit 1" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$o = $result->fetch_object() ;
		$hadthat[] = $o->cid ;
		$q = 'Q'.$o->q ;
		$g = array(
			'id' => $o->cid ,
			'sections' => array () ,
			'controls' => array ()
		) ;
		$g['sections'][] = array ( 'type' => 'wikipage' , 'title' => $o->title , 'wiki' => $o->wiki ) ;
		$g['sections'][] = array ( 'type' => 'item' , 'q' => $q ) ;
		$g['controls'][] = array (
			'type' => 'buttons' ,
			'entries' => array (
				array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => 'Yes' , 'api_action' => array ('action'=>'wbsetsitelink','id'=>$q,'linksite'=>$o->wiki,'linktitle'=>$o->title) ) ,
				array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip' ) ,
				array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => 'No' )
			)
		) ;
		$out['tiles'][] = $g ;
	}
	
	if ( $wiki == 'specieswiki' ) {
		$wil = new WikidataItemList ;
		$qs = array('P428','P835') ;
		foreach ( $out['tiles'] AS $tile ) $qs[] = $tile['sections'][1]['q'] ;
		$wil->loadItems ( $qs ) ;
		foreach ( $out['tiles'] AS $k => $tile ) {
			$q = $tile['sections'][1]['q'] ;
			if ( $testing ) print "\n$q\n" ;
			if ( !$wil->hasItem($q) ) continue ;
			$i = $wil->getItem($q) ;
			$parts = array() ;
			foreach ( array('P428','P835') AS $prop ) {
				if ( !$i->hasClaims($prop) ) continue ;
				if ( $testing ) print_r ( $i ) ;
				$parts[] = $wil->getItem($prop)->getLabel($lang) . ": " . $i->getStrings($prop)[0] ;
			}
			if ( count($parts) == 0 ) continue ;
			$out['tiles'][$k]['sections'][] = array ( 'type' => 'text' , 'text' => implode("\n",$parts) ) ;
		}
	}
	
} else if ( $_REQUEST['action'] == 'log_action' ) {

	$decision = get_request ( 'decision' , '' ) ;
	$id = get_request('tile',-1)*1 ;
	$sql = "UPDATE candidates SET checked=1 WHERE id=$id" ;
	if(!$result = $db->query($sql)) $out['error'] = 'There was an error running the query [' . $db->error . ']';
	$out['sql'] = $sql ;
	
	if ( $decision == 'yes' ) {
		$sql = "SELECT no_wd_id FROM candidates WHERE id=$id" ;
		if(!$result = $db->query($sql)) $out['error'] = 'There was an error running the query [' . $db->error . ']';
		$no_wd_id = '' ;
		while($o = $result->fetch_object()) $no_wd_id = $o->no_wd_id ;
		$sql = "UPDATE candidates SET checked=1 WHERE no_wd_id=$no_wd_id" ;
		if(!$result = $db->query($sql)) $out['error'] = 'There was an error running the query [' . $db->error . ']';
	}
	
	
} else {
	$out['error'] = "No valid action!" ;
}

print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>