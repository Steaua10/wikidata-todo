#!/usr/bin/php
<?PHP


$store_labels = false ;

// S=string; T=time; C=coordinate; no letter=item

if ( 0 ) {
	unlink ( 'test.sqlite' ) ;
	$sqlite = new SQLite3 ( 'test.sqlite' ) ;
} else {
	$sqlite = new SQLite3 ( ':memory:' ) ;
}
$sqlite->exec ( "CREATE TABLE text ( item TINYTEXT , type TINYTEXT , language TINYTEXT , content MEDIUMTEXT )" ) ;
$sqlite->exec ( "CREATE TABLE c_item ( property TINYTEXT , item TINYTEXT , content MEDIUMTEXT )" ) ;
$sqlite->exec ( "CREATE TABLE c_string ( property TINYTEXT , item TINYTEXT , content MEDIUMTEXT )" ) ;
$sqlite->exec ( "CREATE TABLE c_time ( property TINYTEXT , item TINYTEXT , time TINYTEXT,timezone TINYTEXT,before TINYTEXT,after TINYTEXT,precision TINYTEXT,calendarmodel TINYTEXT )" ) ;
$sqlite->exec ( "CREATE TABLE c_coord ( property TINYTEXT , item TINYTEXT , latitude TINYTEXT,longitude TINYTEXT,altitude TINYTEXT,precision TINYTEXT,globe TINYTEXT )" ) ;
$sqlite->exec ( "CREATE TABLE c_special ( property TINYTEXT , item TINYTEXT , content MEDIUMTEXT )" ) ;
//$sqlite->exec ( "" ) ;

$title = '' ;
$ns = '' ;
$last_text = '' ;
while ( !feof ( STDIN ) ) {
        $line = trim(fgets(STDIN));
        if ( preg_match ( '/<title>Q(.+)<\/title>/' , $line , $m ) ) $title = $m[1] ;
        else if ( preg_match ( '/<title>Property:(P.+)<\/title>/' , $line , $m ) ) $title = $m[1] ;
        else if ( preg_match ( '/<ns>(.+)<\/ns>/' , $line , $m ) ) $ns = $m[1] ;
        else if ( ( $ns == 0 || $ns == 120 ) && preg_match ( '/<text .+?>(.*)<\/text>/' , $line , $m ) ) $last_text = $m[1] ;
        else if ( preg_match ( '/<\/page>/' , $line , $m ) and $last_text != '' ) {
                $json = json_decode ( html_entity_decode ( $last_text ) ) ;
                $last_text = '' ;
                if ( $store_labels && isset ( $json->label ) ) {
                        foreach ( $json->label AS $wiki => $text ) {
                        	$sql = "INSERT INTO text (item,type,language,content) VALUES ('$title','label','$wiki','" . $sqlite->escapeString ( $text ) . "')" ;
                        	$sqlite->exec ( $sql ) ;
#                        	print "$title\tL:$wiki\t$text\n" ;
                        }
                }
                if ( $ns == 0 && isset ( $json->claims ) ) { // Items only
                        foreach ( $json->claims AS $v ) {
                                if ( isset ( $v->m[3]->{'numeric-id'} ) ) {
										$sql = "INSERT INTO c_item (property,item,content) VALUES ('".$v->m[1]."','$title','".$sqlite->escapeString($v->m[3]->{'numeric-id'})."')" ;
										$sqlite->exec ( $sql ) ;
#                                        $s = "$title\t" . $v->m[1] . "\t" . $v->m[3]->{'numeric-id'} . "\n" ; ;
#                                        print $s ;
                                } else if ( isset ( $v->m[2] ) and $v->m[2] == 'bad' ) {
                                        // Ignore
                                } else if ( isset ( $v->m[2] ) and $v->m[2] == 'string' ) {
										$sql = "INSERT INTO c_string (property,item,content) VALUES ('".$v->m[1]."','$title','".$sqlite->escapeString($v->m[3])."')" ;
										$sqlite->exec ( $sql ) ;
#                                        $s = "$title\t" . $v->m[1] . "\tS" . trim($v->m[3]) . "\n" ; ;
#                                        print $s ;
                                } else if ( isset ( $v->m[2] ) and $v->m[2] == 'time' ) {
										$sql = "INSERT INTO c_time (property,item,time,timezone,before,after,precision,calendarmodel) VALUES ('".$v->m[1]."','$title'," .
											"'".$sqlite->escapeString($v->m[3]->time)."'," .
											"'".$sqlite->escapeString($v->m[3]->timezone)."'," .
											"'".$sqlite->escapeString($v->m[3]->before)."'," .
											"'".$sqlite->escapeString($v->m[3]->after)."'," .
											"'".$sqlite->escapeString($v->m[3]->precision)."'," .
											"'".$sqlite->escapeString($v->m[3]->calendarmodel)."')" ;
										$sqlite->exec ( $sql ) ;
/*                                        $s = "$title\t" . $v->m[1] . "\tT" ;
                                        $s .= '|' . trim($v->m[3]->time) ;
                                        $s .= '|' . trim($v->m[3]->timezone) ;
                                        $s .= '|' . trim($v->m[3]->before) ;
                                        $s .= '|' . trim($v->m[3]->after) ;
                                        $s .= '|' . trim($v->m[3]->precision) ;
                                        $s .= '|' . trim(preg_replace('/^.+\/Q/','',$v->m[3]->calendarmodel)) ; // Item ID
                                        $s .= "\n" ;
                                        print $s ;*/
                                } else if ( isset ( $v->m[2] ) and $v->m[2] == 'globecoordinate' ) {
										$sql = "INSERT INTO c_coord (property,item,latitude,longitude,altitude,precision,globe) VALUES ('".$v->m[1]."','$title',".
											"'".$sqlite->escapeString($v->m[3]->latitude)."'," . 
											"'".$sqlite->escapeString($v->m[3]->longitude)."'," . 
											"'".$sqlite->escapeString($v->m[3]->altitude)."'," . 
											"'".$sqlite->escapeString($v->m[3]->precision)."'," . 
											"'".$sqlite->escapeString($v->m[3]->globe)."')" ;
										$sqlite->exec ( $sql ) ;
/*                                        $s = "$title\t" . $v->m[1] . "\tC" ;
                                        $s .= '|' . trim($v->m[3]->latitude) ;
                                        $s .= '|' . trim($v->m[3]->longitude) ;
                                        $s .= '|' . trim($v->m[3]->altitude) ;
                                        $s .= '|' . trim($v->m[3]->precision) ;
                                        $s .= '|' . trim(preg_replace('/^.+\/Q/','',$v->m[3]->globe)) ; // Item ID
                                        $s .= "\n" ;
                                        print $s ;*/
                                } else if ( $v->m[0] == 'novalue' ) {
										$sql = "INSERT INTO c_special (property,item,content) VALUES ('".$v->m[1]."','$title','novalue')" ;
										$sqlite->exec ( $sql ) ;
//                                        $s = "$title\t" . $v->m[1] . "\t4294967295\n" ; // MAX_UINT32-1
//                                        print $s ;
                                } else if ( $v->m[0] == 'somevalue' ) {
										$sql = "INSERT INTO c_special (property,item,content) VALUES ('".$v->m[1]."','$title','somevalue')" ;
										$sqlite->exec ( $sql ) ;
//                                        $s = "$title\t" . $v->m[1] . "\t4294967294\n" ; // MAX_UINT32-2
//                                        print $s ;
                                } else {
                                        fwrite ( STDERR , json_encode ( $v )."\n" ) ; // STDERR mismatching claim
                                }
                        }
                }
        }
}

$sqlite->exec ( "CREATE INDEX c_item_i1 on c_item ( property , content ) " ) ;
$sqlite->exec ( "CREATE INDEX c_string_i1 on c_string ( property , content ) " ) ;
$sqlite->exec ( "CREATE INDEX c_special_i1 on c_special ( property , content ) " ) ;
$sqlite->exec ( "CREATE INDEX c_time_i1 on c_time ( property , time ) " ) ;
$sqlite->exec ( "CREATE INDEX c_coord_i1 on c_coord ( property , latitude,longitude ) " ) ;

$sqlite->exec ( "CREATE INDEX text_i1 on text ( item , type , language ) " ) ;

print "Done\n" ;
while ( 1 ) ;

?>