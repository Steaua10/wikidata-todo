#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( "public_html/php/common.php" ) ;

$dbu = openToolDB ( 'monitor_p' ) ;

$dir = "/data/project/wikidata-todo/c2r" ;
exec ( "rm -f $dir/*" ) ;

$cc = array() ;
$sql = "SELECT DISTINCT country_code FROM country ORDER BY country_code" ;
if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $dbu->error . ']');
while($o = $result->fetch_object())$cc[] = $o->country_code ;

$headers = array() ;
$sql = "SELECT DISTINCT `key` FROM country ORDER BY `key`" ;
if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $dbu->error . ']');
while($o = $result->fetch_object()) $headers[] = $o->key ;


$fpa = fopen("$dir/all.tab", 'w');
$s = "country\tday\t" . implode ( "\t" , $headers ) ;
fwrite ( $fpa , "$s\n" ) ;

foreach ( $cc AS $c ) {
	$days = array() ;
	$sql = "SELECT * FROM country WHERE country_code='$c'" ;
	if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $dbu->error . ']');
	while($o = $result->fetch_object()) {
		$days[$o->day][$o->key] = $o->items ;
	}
	
	ksort ( $days ) ;
	$fp = fopen("$dir/$c.tab", 'w');
	$s = "day\t" . implode ( "\t" , $headers ) ;
	fwrite ( $fp , "$s\n" ) ;
	foreach ( $days AS $day => $d ) {
		$s = '' ;
		foreach ( $headers AS $h ) $s .= "\t" . ( isset($d[$h]) ? $d[$h] : '' ) ;
		if ( trim($s) == '' ) continue ;
		$s = substr($day,0,4).'-'.substr($day,4,2).'-'.substr($day,6,2) . $s ;
		fwrite ( $fp , "$s\n" ) ;
		fwrite ( $fpa , "$c\t$s\n" ) ;
	}
	fclose($fp) ;
}
fclose($fpa) ;


?>